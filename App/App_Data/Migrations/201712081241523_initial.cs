namespace App_Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApiLogEntries",
                c => new
                    {
                        ApiLogEntryId = c.Long(nullable: false, identity: true),
                        Application = c.String(),
                        User = c.String(),
                        Machine = c.String(),
                        RequestIpAddress = c.String(),
                        RequestContentType = c.String(),
                        RequestContentBody = c.String(),
                        RequestUri = c.String(),
                        RequestMethod = c.String(),
                        RequestRouteTemplate = c.String(),
                        RequestRouteData = c.String(),
                        RequestHeaders = c.String(),
                        RequestTimestamp = c.DateTime(),
                        ResponseContentType = c.String(),
                        ResponseContentBody = c.String(),
                        ResponseStatusCode = c.Int(),
                        ResponseHeaders = c.String(),
                        ResponseTimestamp = c.DateTime(),
                    })
                .PrimaryKey(t => t.ApiLogEntryId);
            
            CreateTable(
                "dbo.UserTbls",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        DisplayName = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        LastLoggedIn = c.DateTime(),
                        Password = c.String(),
                        SerialNumber = c.String(),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        LastName = c.String(nullable: false, maxLength: 100),
                        Birthdate = c.DateTime(),
                        Telephone = c.String(maxLength: 13),
                        Mobile = c.String(maxLength: 11),
                        Address = c.Byte(),
                        City = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.UserTokenTbls",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        AccessTokenHash = c.String(),
                        AccessTokenExpirationDateTime = c.DateTime(nullable: false),
                        RefreshTokenIdHash = c.String(),
                        Subject = c.String(),
                        RefreshTokenExpiresUtc = c.DateTime(nullable: false),
                        RefreshToken = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserTbls", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserTokenTbls", "UserId", "dbo.UserTbls");
            DropIndex("dbo.UserTokenTbls", new[] { "UserId" });
            DropTable("dbo.UserTokenTbls");
            DropTable("dbo.UserTbls");
            DropTable("dbo.ApiLogEntries");
        }
    }
}
