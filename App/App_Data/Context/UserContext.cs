﻿using DataModel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace App_Data.Context
{
    public partial class UserContext : DbContext
    {
        public virtual DbSet<UserTokenTbl> UserTokenTbl { get; set; }
        public virtual DbSet<UserTbl> IdentityTbl { get; set; }
        public virtual DbSet<ApiLogEntry> ApiLogEntry { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           
            modelBuilder.Entity<UserTbl>()
                 .HasKey(a => a.UserId)
                 .Property(e => e.Birthdate).HasColumnType("datetime");

            modelBuilder.Entity<UserTbl>().Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

            modelBuilder.Entity<UserTbl>().Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100);

            modelBuilder.Entity<UserTbl>().Property(e => e.Mobile).HasMaxLength(11);

            modelBuilder.Entity<UserTbl>().Property(e => e.Telephone).HasMaxLength(13);
            modelBuilder.Entity<UserTbl>().Property(e => e.City).HasMaxLength(100);
            modelBuilder.Entity<UserTbl>().HasMany(a => a.UserTokenTbl)
           .WithRequired(b => b.UserTbl);





            modelBuilder.Entity<UserTokenTbl>()

                .HasKey(e => e.Id);
            modelBuilder.Entity<UserTokenTbl>().HasRequired(d => d.UserTbl)
        .WithMany(p => p.UserTokenTbl)
        .HasForeignKey(d => d.UserId);

        }
    }

 

}
